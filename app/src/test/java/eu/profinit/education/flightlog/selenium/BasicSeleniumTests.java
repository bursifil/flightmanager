package eu.profinit.education.flightlog.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class BasicSeleniumTests {

    private static String URL = "http://localhost:8081";
    private static String DATE_AND_TIME_FLIGHT = "20110020181230";
    private static int WAIT_TIMEOUT = 5;

    private static String TAB_BAR_BUTTON1 = "/html/body/div/div/ul/li[1]/a";
    private static String TAB_BAR_BUTTON2 = "/html/body/div/div/ul/li[2]/a";
    private static String TAB_BAR_BUTTON3 = "/html/body/div/div/ul/li[3]/a";
    private static String LIST_OF_FLIGHTS_IN_AIR = "/html/body/router-view/div/div/div/table/tbody/tr";
    private static String LIST_OF_FLIGHTS_TO_REPORT = "/html/body/router-view/div/table/tbody/tr";
    private static String CONFIRM_LAND = "/html/body/router-view/div/div/div/div/div/div[2]/div[2]/input[1]";
    private static String SHOW_LAND_DIALOG = "/html/body/router-view/div/div/div/table/tbody/tr[position()=1]/td[6]/a[1]";
    private static String FLIGHT_FORM_NO_COPILOT = "/html/body/router-view/div/form/compose[2]/compose/div/div/div/label[3]";
    private static String FLIGHT_FORM_CONFIRM = "/html/body/router-view/div/form/div[2]/div/button";
    private static String FLIGHT_FORM_TAKEOFF = "//*[@id=\"takeoffTime\"]";

    private WebDriver webDriver;

    @Before
    public void setupDriver() {
        webDriver = new ChromeDriver();
    }

    @Test
    public void addFlight() {
        webDriver.get(URL);
        System.out.println("title of page is: " + webDriver.getTitle());

        int numberOfReports = webDriver.findElements(By.xpath(LIST_OF_FLIGHTS_IN_AIR)).size();

        webDriver.findElement(By.xpath(TAB_BAR_BUTTON2)).click();
        webDriver.findElement(By.xpath(FLIGHT_FORM_NO_COPILOT)).click();
        webDriver.findElement(By.xpath(FLIGHT_FORM_TAKEOFF)).sendKeys(DATE_AND_TIME_FLIGHT);
        webDriver.findElement(By.xpath(FLIGHT_FORM_CONFIRM)).click();

        WebDriverWait wait = new WebDriverWait(webDriver, WAIT_TIMEOUT);
        wait.until(ExpectedConditions.alertIsPresent());

        webDriver.switchTo().alert().accept();
        webDriver.findElement(By.xpath(TAB_BAR_BUTTON1)).click();
        int numberOfReportsAfterAdd = webDriver.findElements(By.xpath(LIST_OF_FLIGHTS_IN_AIR)).size();

        assertTrue(numberOfReportsAfterAdd > numberOfReports);
    }

    @Test
    public void land() {
        webDriver.get(URL);
        webDriver.findElement(By.xpath(TAB_BAR_BUTTON3)).click();

        int numberOfReports = webDriver.findElements(By.xpath(LIST_OF_FLIGHTS_TO_REPORT)).size();

        webDriver.findElement(By.xpath(TAB_BAR_BUTTON1)).click();
        webDriver.findElement(By.xpath(SHOW_LAND_DIALOG)).click();
        webDriver.findElement(By.xpath(CONFIRM_LAND)).click();
        webDriver.findElement(By.xpath(TAB_BAR_BUTTON3)).click();

        int numberOfReportsAfterAdd = webDriver.findElements(By.xpath(LIST_OF_FLIGHTS_TO_REPORT)).size();

        assertTrue(numberOfReportsAfterAdd > numberOfReports);
    }

    @After
    public void closeDriver() {
        webDriver.close();
    }

}
