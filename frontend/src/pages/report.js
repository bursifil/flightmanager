import {BackendService} from '../services/backend-service';
import {inject} from 'aurelia-framework';
import moment from 'moment';

@inject(BackendService)
export class Report {

  constructor(backendService) {
    this.backendService = backendService;
    this.flightReport = [];
    this.fromTime = null;
    this.toTime = null;
  }

  activate(params) {
    this.reportUrl = this.backendService.getFlightExportUrl();
    this.backendService.getFlightsForReport()
      .then(data => {
        this.flightReport = data;
      });

  }

  getFormattedFlightDuration(flight) {
    if (flight == null) {
      return "";
    }
    if (flight.landingTime == null) {
      return "--";
    }
    let duration = moment.duration(moment(flight.landingTime).diff(flight.takeoffTime));
    return moment.utc(duration.asMilliseconds()).format("H°mm'");
  }

  openExportFromTo() {
    this.fromTime = new Date();
    this.toTime = new Date();
  }

  closeFromTo() {
    this.fromTime = null;
    this.toTime = null;
  }

  validateDate() {
    var fromTimeDate = new Date(this.fromTime);
    var toTimeDate = new Date(this.toTime);

    console.log(fromTimeDate);
    console.log(toTimeDate);

    if (fromTimeDate > toTimeDate || (!(fromTimeDate instanceof Date) || !(isFinite(fromTimeDate))) ||
    (!(toTimeDate instanceof Date) || !(isFinite(toTimeDate)))) {
        alert("Od musí být menší než do a čas i datum musí být vyplněno!");
    } else {
        document.getElementById("form").submit();
    }
  }

}
