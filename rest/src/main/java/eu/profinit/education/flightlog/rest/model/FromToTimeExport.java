package eu.profinit.education.flightlog.rest.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class FromToTimeExport {
    LocalDateTime fromTime;
    LocalDateTime toTime;
}
