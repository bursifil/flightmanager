package eu.profinit.education.flightlog.service;

import com.sun.istack.NotNull;
import eu.profinit.education.flightlog.domain.repositories.FlightRepository;
import eu.profinit.education.flightlog.to.FileExportTo;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class CsvExportServiceImpl implements CsvExportService {

    private static final String MY_DATE_FORMAT = "dd.MM.yyyy";
    private static final String MY_TIME_FORMAT = "HH:mm:ss";
    private final FlightRepository flightRepository;

    private final String fileName;

    public CsvExportServiceImpl(FlightRepository flightRepository, @Value("${csv.export.flight.fileName}") String fileName) {
        this.flightRepository = flightRepository;
        this.fileName = fileName;
    }

    @Override
    public FileExportTo getAllFlightsAsCsvTimeConstraint(LocalDate fromTime, LocalDate toTime) {
        Path path = Paths.get(fileName);
        MediaType csvType = new MediaType("text", "csv");
        byte[] out = null;

        try {
            BufferedWriter writer = Files.newBufferedWriter(path);
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                    .withHeader("Datum", "Typ", "Imatrikulace", "Pilot", "Host/Clen klubu", "Adresa", "Copilot", "Úkol", "Start", "Přistání", "Doba letu"));

            flightRepository.findAll(Sort.by(Sort.Order.asc("takeoffTime"), Sort.Order.asc("id")))
                    .stream()
                    .filter(s -> s.getLandingTime() != null)
                    .forEach(flight -> {
                        try {
                            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(MY_DATE_FORMAT);
                            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(MY_TIME_FORMAT);



                            if (!flight.getTakeoffTime().toLocalDate().isBefore(fromTime) && !flight.getTakeoffTime().toLocalDate().isAfter(toTime)) {
                                csvPrinter.printRecord(flight.getTakeoffTime().format(dateFormatter), flight.getFlightType(),
                                        flight.getAirplane().getSafeImmatriculation(), flight.getPilot().getFullName(), flight.getPilot().getPersonType(),
                                        flight.getPilot().getFullAdress(), (flight.getCopilot() == null ? "" : flight.getCopilot().getFullName()), flight.getTask().getValue(), flight.getTakeoffTime().format(timeFormatter),
                                        flight.getLandingTime().format(timeFormatter), flight.getFlightDuration() + " min");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });

            csvPrinter.flush();
            out = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new FileExportTo(fileName, csvType, out);
    }

    @Override
    public FileExportTo getAllFlightsAsCsv() {
        Path path = Paths.get(fileName);
        MediaType csvType = new MediaType("text", "csv");
        byte[] out = null;

        try {
            BufferedWriter writer = Files.newBufferedWriter(path);
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                .withHeader("Datum", "Typ", "Imatrikulace", "Pilot", "Host/Clen klubu", "Adresa", "Copilot", "Úkol", "Start", "Přistání", "Doba letu"));

            flightRepository.findAll(Sort.by(Sort.Order.asc("takeoffTime"), Sort.Order.asc("id")))
                .stream()
                .filter(s -> s.getLandingTime() != null)
                .forEach(flight -> {
                    try {
                        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(MY_DATE_FORMAT);
                        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(MY_TIME_FORMAT);

                        csvPrinter.printRecord(flight.getTakeoffTime().format(dateFormatter), flight.getFlightType(),
                                flight.getAirplane().getSafeImmatriculation(), flight.getPilot().getFullName(), flight.getPilot().getPersonType(),
                                flight.getPilot().getFullAdress(), (flight.getCopilot() == null ? "" : flight.getCopilot().getFullName()), flight.getTask().getValue(), flight.getTakeoffTime().format(timeFormatter),
                                flight.getLandingTime().format(timeFormatter), flight.getFlightDuration() + " min");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

            csvPrinter.flush();
            out = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new FileExportTo(fileName, csvType, out);
    }
}
