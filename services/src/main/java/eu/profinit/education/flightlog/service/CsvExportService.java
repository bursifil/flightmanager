package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.to.FileExportTo;

import java.time.LocalDate;

public interface CsvExportService {

    FileExportTo getAllFlightsAsCsv();

    FileExportTo getAllFlightsAsCsvTimeConstraint(LocalDate fromTime, LocalDate toTime);

}
