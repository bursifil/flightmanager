package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.IntegrationTestConfig;
import eu.profinit.education.flightlog.to.FileExportTo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IntegrationTestConfig.class)
@Transactional
@TestPropertySource(
    locations = "classpath:application-integrationtest.properties")
public class CsvExportServiceTest {

    private final String FILE_NAME_EXPECTED_OUTPUT = "csv/test_flights.csv";

    @Autowired
    private CsvExportService testSubject;

    @Test
    public void testCSVExport() throws IOException, URISyntaxException {
        FileExportTo allFlightsAsCsv = testSubject.getAllFlightsAsCsv();

        String stringFileCreated = new String(allFlightsAsCsv.getContent());
        String stringFileExpected = readFileToString(FILE_NAME_EXPECTED_OUTPUT);
        String currentFileType = allFlightsAsCsv.getContentType().getType() + "/" + allFlightsAsCsv.getContentType().getSubtype();
        String expectedFileType = readFileType(FILE_NAME_EXPECTED_OUTPUT);

        assertEquals(stringFileCreated, stringFileExpected);
        assertEquals(currentFileType, expectedFileType);
    }

    private String readFileToString(String filename) throws IOException, URISyntaxException {
        return new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource(filename).toURI())));
    }

    private String readFileType(String filename) throws IOException {
        return Files.probeContentType(Paths.get(filename));
    }

}