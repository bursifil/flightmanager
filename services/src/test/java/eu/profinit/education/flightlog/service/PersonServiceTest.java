package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.dao.ClubDatabaseDao;
import eu.profinit.education.flightlog.dao.User;
import eu.profinit.education.flightlog.domain.entities.Person;
import eu.profinit.education.flightlog.domain.repositories.PersonRepository;
import eu.profinit.education.flightlog.to.AddressTo;
import eu.profinit.education.flightlog.to.PersonTo;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @Mock
    private ClubDatabaseDao clubDatabaseDao;

    private PersonServiceImpl testSubject;

    @Before
    public void setUp(){
        testSubject = new PersonServiceImpl(personRepository, clubDatabaseDao);
    }

    @Test
    public void shouldCreateGuest() {
        // prepare data
        PersonTo guestToCreate = PersonTo.builder()
            .firstName("Jan")
            .lastName("Novák")
            .address(AddressTo.builder()
                .street("Tychonova 2")
                .city("Praha 6")
                .postalCode("16000")
                .build())
            .build();

        // mock behaviour
        when(personRepository.save(any())).thenAnswer(AdditionalAnswers.returnsFirstArg());

        // call tested method
        Person person = testSubject.getExistingOrCreatePerson(guestToCreate);

        // verify results
        assertEquals("Person type does not match", Person.Type.GUEST, person.getPersonType());
        assertEquals("First name does not match", guestToCreate.getFirstName(), person.getFirstName());
        assertEquals("Last name does not match", guestToCreate.getLastName(), person.getLastName());

        assertEquals("Strear does not match", guestToCreate.getAddress().getStreet(), person.getAddress().getStreet());

    }

    @Test
    public void shouldReturnExistingClubMember() {
        // prepare data
        PersonTo existingClubMember = PersonTo.builder()
            .memberId(2L)
            .build();

        User testUser = new User(2L, "Kamila", "Spoustová", Arrays.asList("PILOT"));
        Person clubMemberFromDd = Person.builder().personType(Person.Type.CLUB_MEMBER).memberId(2L).build();

        // mock behaviour
        when(personRepository.findByMemberId(2L)).thenReturn(Optional.of(clubMemberFromDd));
        when(clubDatabaseDao.getUsers()).thenReturn(Arrays.asList(testUser));


        // call tested method
        Person person = testSubject.getExistingOrCreatePerson(existingClubMember);

        // verify results
        assertTrue("Should return prepared instance", clubMemberFromDd == person);

    }

    @Test
    public void shouldCreateNewClubMember() {
        // TODO 7.1: Naimplementujte unit test s pouzitim mocku
        // prepare data
        PersonTo memberToCreate = PersonTo.builder()
                .firstName("Karel")
                .lastName("Novotny")
                .memberId(12L)
                .address(AddressTo.builder()
                        .street("Litomericka 2")
                        .city("Praha 1")
                        .postalCode("19000")
                        .build())
                .build();

        User user = User.builder()
                .memberId(memberToCreate.getMemberId())
                .firstName(memberToCreate.getFirstName())
                .lastName(memberToCreate.getLastName())
                .build();

        // mock behaviour
        List<User> mockUsers = new ArrayList<>();
        mockUsers.add(user);

        //when(personRepository.findByMemberId(12L)).thenReturn(Optional.empty());
        when(clubDatabaseDao.getUsers()).thenReturn(mockUsers);
        when(personRepository.save(any())).thenAnswer(AdditionalAnswers.returnsFirstArg());

        // call tested method
        Person person = testSubject.getExistingOrCreatePerson(memberToCreate);

        // verify results
        assertEquals("Person type does not match", Person.Type.CLUB_MEMBER, person.getPersonType());
        assertEquals("First name does not match", memberToCreate.getFirstName(), person.getFirstName());
        assertEquals("Last name does not match", memberToCreate.getLastName(), person.getLastName());
        assertEquals("MemberID doest not march", memberToCreate.getMemberId(), person.getMemberId());

        Mockito.verify(personRepository, times(1)).save(person);
    }

}